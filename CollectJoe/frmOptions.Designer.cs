﻿namespace CollectJoe
{
    partial class frmOptions
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gpxGameField = new System.Windows.Forms.GroupBox();
            this.lblGameField = new System.Windows.Forms.Label();
            this.nudFieldBoxesV = new System.Windows.Forms.NumericUpDown();
            this.nudFieldBoxesH = new System.Windows.Forms.NumericUpDown();
            this.lblFieldBoxesV = new System.Windows.Forms.Label();
            this.lblFieldBoxesH = new System.Windows.Forms.Label();
            this.gpxColors = new System.Windows.Forms.GroupBox();
            this.btnColorsBoxType3 = new System.Windows.Forms.Button();
            this.btnColorsBoxType2 = new System.Windows.Forms.Button();
            this.btnColorsBoxType1 = new System.Windows.Forms.Button();
            this.lblColorsBoxType3 = new System.Windows.Forms.Label();
            this.lblColorsBoxType2 = new System.Windows.Forms.Label();
            this.lblColorsBoxType1 = new System.Windows.Forms.Label();
            this.btnColorsGameField = new System.Windows.Forms.Button();
            this.btnColorsBoxes = new System.Windows.Forms.Button();
            this.lblColorsGameField = new System.Windows.Forms.Label();
            this.lblColorBoxes = new System.Windows.Forms.Label();
            this.lblColors = new System.Windows.Forms.Label();
            this.gpxScoring = new System.Windows.Forms.GroupBox();
            this.nudScoringBoxType3 = new System.Windows.Forms.NumericUpDown();
            this.nudScoringBoxType2 = new System.Windows.Forms.NumericUpDown();
            this.nudScoringBoxType1 = new System.Windows.Forms.NumericUpDown();
            this.lblScoringBoxType3 = new System.Windows.Forms.Label();
            this.lblScoringBoxType2 = new System.Windows.Forms.Label();
            this.lblScoringBoxType1 = new System.Windows.Forms.Label();
            this.lblScoring = new System.Windows.Forms.Label();
            this.lblSettings = new System.Windows.Forms.Label();
            this.btnApply = new System.Windows.Forms.Button();
            this.cldColorPicker = new System.Windows.Forms.ColorDialog();
            this.gpxGame = new System.Windows.Forms.GroupBox();
            this.lblGameFanfare = new System.Windows.Forms.Label();
            this.chbGameFanfare = new System.Windows.Forms.CheckBox();
            this.nudGameDuration = new System.Windows.Forms.NumericUpDown();
            this.lblGameDuration = new System.Windows.Forms.Label();
            this.lblGame = new System.Windows.Forms.Label();
            this.gpxGameField.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudFieldBoxesV)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudFieldBoxesH)).BeginInit();
            this.gpxColors.SuspendLayout();
            this.gpxScoring.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudScoringBoxType3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudScoringBoxType2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudScoringBoxType1)).BeginInit();
            this.gpxGame.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudGameDuration)).BeginInit();
            this.SuspendLayout();
            // 
            // gpxGameField
            // 
            this.gpxGameField.Controls.Add(this.lblGameField);
            this.gpxGameField.Controls.Add(this.nudFieldBoxesV);
            this.gpxGameField.Controls.Add(this.nudFieldBoxesH);
            this.gpxGameField.Controls.Add(this.lblFieldBoxesV);
            this.gpxGameField.Controls.Add(this.lblFieldBoxesH);
            this.gpxGameField.Location = new System.Drawing.Point(12, 118);
            this.gpxGameField.Name = "gpxGameField";
            this.gpxGameField.Size = new System.Drawing.Size(291, 128);
            this.gpxGameField.TabIndex = 0;
            this.gpxGameField.TabStop = false;
            // 
            // lblGameField
            // 
            this.lblGameField.AutoSize = true;
            this.lblGameField.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGameField.Location = new System.Drawing.Point(6, 16);
            this.lblGameField.Name = "lblGameField";
            this.lblGameField.Size = new System.Drawing.Size(147, 37);
            this.lblGameField.TabIndex = 0;
            this.lblGameField.Text = "Spielfeld";
            // 
            // nudFieldBoxesV
            // 
            this.nudFieldBoxesV.Location = new System.Drawing.Point(165, 100);
            this.nudFieldBoxesV.Name = "nudFieldBoxesV";
            this.nudFieldBoxesV.Size = new System.Drawing.Size(120, 20);
            this.nudFieldBoxesV.TabIndex = 1;
            this.nudFieldBoxesV.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // nudFieldBoxesH
            // 
            this.nudFieldBoxesH.Location = new System.Drawing.Point(165, 74);
            this.nudFieldBoxesH.Name = "nudFieldBoxesH";
            this.nudFieldBoxesH.Size = new System.Drawing.Size(120, 20);
            this.nudFieldBoxesH.TabIndex = 0;
            this.nudFieldBoxesH.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // lblFieldBoxesV
            // 
            this.lblFieldBoxesV.AutoSize = true;
            this.lblFieldBoxesV.Location = new System.Drawing.Point(10, 102);
            this.lblFieldBoxesV.Name = "lblFieldBoxesV";
            this.lblFieldBoxesV.Size = new System.Drawing.Size(74, 13);
            this.lblFieldBoxesV.TabIndex = 0;
            this.lblFieldBoxesV.Text = "Boxen vertikal";
            // 
            // lblFieldBoxesH
            // 
            this.lblFieldBoxesH.AutoSize = true;
            this.lblFieldBoxesH.Location = new System.Drawing.Point(10, 76);
            this.lblFieldBoxesH.Name = "lblFieldBoxesH";
            this.lblFieldBoxesH.Size = new System.Drawing.Size(85, 13);
            this.lblFieldBoxesH.TabIndex = 0;
            this.lblFieldBoxesH.Text = "Boxen horizontal";
            // 
            // gpxColors
            // 
            this.gpxColors.Controls.Add(this.btnColorsBoxType3);
            this.gpxColors.Controls.Add(this.btnColorsBoxType2);
            this.gpxColors.Controls.Add(this.btnColorsBoxType1);
            this.gpxColors.Controls.Add(this.lblColorsBoxType3);
            this.gpxColors.Controls.Add(this.lblColorsBoxType2);
            this.gpxColors.Controls.Add(this.lblColorsBoxType1);
            this.gpxColors.Controls.Add(this.btnColorsGameField);
            this.gpxColors.Controls.Add(this.btnColorsBoxes);
            this.gpxColors.Controls.Add(this.lblColorsGameField);
            this.gpxColors.Controls.Add(this.lblColorBoxes);
            this.gpxColors.Controls.Add(this.lblColors);
            this.gpxColors.Location = new System.Drawing.Point(314, 118);
            this.gpxColors.Name = "gpxColors";
            this.gpxColors.Size = new System.Drawing.Size(253, 215);
            this.gpxColors.TabIndex = 2;
            this.gpxColors.TabStop = false;
            // 
            // btnColorsBoxType3
            // 
            this.btnColorsBoxType3.BackColor = System.Drawing.Color.Red;
            this.btnColorsBoxType3.Location = new System.Drawing.Point(172, 184);
            this.btnColorsBoxType3.Name = "btnColorsBoxType3";
            this.btnColorsBoxType3.Size = new System.Drawing.Size(75, 23);
            this.btnColorsBoxType3.TabIndex = 4;
            this.btnColorsBoxType3.UseVisualStyleBackColor = false;
            this.btnColorsBoxType3.Click += new System.EventHandler(this.BtnColors_Click);
            // 
            // btnColorsBoxType2
            // 
            this.btnColorsBoxType2.BackColor = System.Drawing.Color.Yellow;
            this.btnColorsBoxType2.Location = new System.Drawing.Point(172, 155);
            this.btnColorsBoxType2.Name = "btnColorsBoxType2";
            this.btnColorsBoxType2.Size = new System.Drawing.Size(75, 23);
            this.btnColorsBoxType2.TabIndex = 3;
            this.btnColorsBoxType2.UseVisualStyleBackColor = false;
            this.btnColorsBoxType2.Click += new System.EventHandler(this.BtnColors_Click);
            // 
            // btnColorsBoxType1
            // 
            this.btnColorsBoxType1.BackColor = System.Drawing.Color.SteelBlue;
            this.btnColorsBoxType1.Location = new System.Drawing.Point(172, 126);
            this.btnColorsBoxType1.Name = "btnColorsBoxType1";
            this.btnColorsBoxType1.Size = new System.Drawing.Size(75, 23);
            this.btnColorsBoxType1.TabIndex = 2;
            this.btnColorsBoxType1.UseVisualStyleBackColor = false;
            this.btnColorsBoxType1.Click += new System.EventHandler(this.BtnColors_Click);
            // 
            // lblColorsBoxType3
            // 
            this.lblColorsBoxType3.AutoSize = true;
            this.lblColorsBoxType3.Location = new System.Drawing.Point(11, 189);
            this.lblColorsBoxType3.Name = "lblColorsBoxType3";
            this.lblColorsBoxType3.Size = new System.Drawing.Size(51, 13);
            this.lblColorsBoxType3.TabIndex = 0;
            this.lblColorsBoxType3.Text = "3. Boxtyp";
            // 
            // lblColorsBoxType2
            // 
            this.lblColorsBoxType2.AutoSize = true;
            this.lblColorsBoxType2.Location = new System.Drawing.Point(11, 160);
            this.lblColorsBoxType2.Name = "lblColorsBoxType2";
            this.lblColorsBoxType2.Size = new System.Drawing.Size(51, 13);
            this.lblColorsBoxType2.TabIndex = 0;
            this.lblColorsBoxType2.Text = "2. Boxtyp";
            // 
            // lblColorsBoxType1
            // 
            this.lblColorsBoxType1.AutoSize = true;
            this.lblColorsBoxType1.Location = new System.Drawing.Point(11, 131);
            this.lblColorsBoxType1.Name = "lblColorsBoxType1";
            this.lblColorsBoxType1.Size = new System.Drawing.Size(51, 13);
            this.lblColorsBoxType1.TabIndex = 0;
            this.lblColorsBoxType1.Text = "1. Boxtyp";
            // 
            // btnColorsGameField
            // 
            this.btnColorsGameField.BackColor = System.Drawing.Color.YellowGreen;
            this.btnColorsGameField.Location = new System.Drawing.Point(172, 97);
            this.btnColorsGameField.Name = "btnColorsGameField";
            this.btnColorsGameField.Size = new System.Drawing.Size(75, 23);
            this.btnColorsGameField.TabIndex = 1;
            this.btnColorsGameField.UseVisualStyleBackColor = false;
            this.btnColorsGameField.Click += new System.EventHandler(this.BtnColors_Click);
            // 
            // btnColorsBoxes
            // 
            this.btnColorsBoxes.BackColor = System.Drawing.Color.DarkGray;
            this.btnColorsBoxes.Location = new System.Drawing.Point(172, 71);
            this.btnColorsBoxes.Name = "btnColorsBoxes";
            this.btnColorsBoxes.Size = new System.Drawing.Size(75, 23);
            this.btnColorsBoxes.TabIndex = 0;
            this.btnColorsBoxes.UseVisualStyleBackColor = false;
            this.btnColorsBoxes.Click += new System.EventHandler(this.BtnColors_Click);
            // 
            // lblColorsGameField
            // 
            this.lblColorsGameField.AutoSize = true;
            this.lblColorsGameField.Location = new System.Drawing.Point(11, 102);
            this.lblColorsGameField.Name = "lblColorsGameField";
            this.lblColorsGameField.Size = new System.Drawing.Size(47, 13);
            this.lblColorsGameField.TabIndex = 0;
            this.lblColorsGameField.Text = "Spielfeld";
            // 
            // lblColorBoxes
            // 
            this.lblColorBoxes.AutoSize = true;
            this.lblColorBoxes.Location = new System.Drawing.Point(11, 76);
            this.lblColorBoxes.Name = "lblColorBoxes";
            this.lblColorBoxes.Size = new System.Drawing.Size(37, 13);
            this.lblColorBoxes.TabIndex = 0;
            this.lblColorBoxes.Text = "Boxen";
            // 
            // lblColors
            // 
            this.lblColors.AutoSize = true;
            this.lblColors.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblColors.Location = new System.Drawing.Point(7, 16);
            this.lblColors.Name = "lblColors";
            this.lblColors.Size = new System.Drawing.Size(125, 37);
            this.lblColors.TabIndex = 0;
            this.lblColors.Text = "Farben";
            // 
            // gpxScoring
            // 
            this.gpxScoring.Controls.Add(this.nudScoringBoxType3);
            this.gpxScoring.Controls.Add(this.nudScoringBoxType2);
            this.gpxScoring.Controls.Add(this.nudScoringBoxType1);
            this.gpxScoring.Controls.Add(this.lblScoringBoxType3);
            this.gpxScoring.Controls.Add(this.lblScoringBoxType2);
            this.gpxScoring.Controls.Add(this.lblScoringBoxType1);
            this.gpxScoring.Controls.Add(this.lblScoring);
            this.gpxScoring.Location = new System.Drawing.Point(12, 252);
            this.gpxScoring.Name = "gpxScoring";
            this.gpxScoring.Size = new System.Drawing.Size(291, 172);
            this.gpxScoring.TabIndex = 1;
            this.gpxScoring.TabStop = false;
            // 
            // nudScoringBoxType3
            // 
            this.nudScoringBoxType3.Location = new System.Drawing.Point(163, 140);
            this.nudScoringBoxType3.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            -2147483648});
            this.nudScoringBoxType3.Name = "nudScoringBoxType3";
            this.nudScoringBoxType3.Size = new System.Drawing.Size(120, 20);
            this.nudScoringBoxType3.TabIndex = 2;
            this.nudScoringBoxType3.Value = new decimal(new int[] {
            50,
            0,
            0,
            -2147483648});
            // 
            // nudScoringBoxType2
            // 
            this.nudScoringBoxType2.Location = new System.Drawing.Point(164, 111);
            this.nudScoringBoxType2.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            -2147483648});
            this.nudScoringBoxType2.Name = "nudScoringBoxType2";
            this.nudScoringBoxType2.Size = new System.Drawing.Size(120, 20);
            this.nudScoringBoxType2.TabIndex = 1;
            this.nudScoringBoxType2.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // nudScoringBoxType1
            // 
            this.nudScoringBoxType1.Location = new System.Drawing.Point(164, 82);
            this.nudScoringBoxType1.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            -2147483648});
            this.nudScoringBoxType1.Name = "nudScoringBoxType1";
            this.nudScoringBoxType1.Size = new System.Drawing.Size(120, 20);
            this.nudScoringBoxType1.TabIndex = 0;
            this.nudScoringBoxType1.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // lblScoringBoxType3
            // 
            this.lblScoringBoxType3.AutoSize = true;
            this.lblScoringBoxType3.Location = new System.Drawing.Point(15, 144);
            this.lblScoringBoxType3.Name = "lblScoringBoxType3";
            this.lblScoringBoxType3.Size = new System.Drawing.Size(51, 13);
            this.lblScoringBoxType3.TabIndex = 0;
            this.lblScoringBoxType3.Text = "3. Boxtyp";
            // 
            // lblScoringBoxType2
            // 
            this.lblScoringBoxType2.AutoSize = true;
            this.lblScoringBoxType2.Location = new System.Drawing.Point(15, 115);
            this.lblScoringBoxType2.Name = "lblScoringBoxType2";
            this.lblScoringBoxType2.Size = new System.Drawing.Size(51, 13);
            this.lblScoringBoxType2.TabIndex = 0;
            this.lblScoringBoxType2.Text = "2. Boxtyp";
            // 
            // lblScoringBoxType1
            // 
            this.lblScoringBoxType1.AutoSize = true;
            this.lblScoringBoxType1.Location = new System.Drawing.Point(15, 86);
            this.lblScoringBoxType1.Name = "lblScoringBoxType1";
            this.lblScoringBoxType1.Size = new System.Drawing.Size(51, 13);
            this.lblScoringBoxType1.TabIndex = 0;
            this.lblScoringBoxType1.Text = "1. Boxtyp";
            // 
            // lblScoring
            // 
            this.lblScoring.AutoSize = true;
            this.lblScoring.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblScoring.Location = new System.Drawing.Point(6, 16);
            this.lblScoring.Name = "lblScoring";
            this.lblScoring.Size = new System.Drawing.Size(183, 37);
            this.lblScoring.TabIndex = 0;
            this.lblScoring.Text = "Wertungen";
            // 
            // lblSettings
            // 
            this.lblSettings.AutoSize = true;
            this.lblSettings.Font = new System.Drawing.Font("Microsoft Sans Serif", 32F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSettings.Location = new System.Drawing.Point(12, 34);
            this.lblSettings.Name = "lblSettings";
            this.lblSettings.Size = new System.Drawing.Size(296, 51);
            this.lblSettings.TabIndex = 0;
            this.lblSettings.Text = "Einstellungen";
            // 
            // btnApply
            // 
            this.btnApply.AutoSize = true;
            this.btnApply.Location = new System.Drawing.Point(12, 444);
            this.btnApply.Name = "btnApply";
            this.btnApply.Size = new System.Drawing.Size(78, 23);
            this.btnApply.TabIndex = 4;
            this.btnApply.Text = "Ü&bernehmen";
            this.btnApply.UseVisualStyleBackColor = true;
            this.btnApply.Click += new System.EventHandler(this.BtnApply_Click);
            // 
            // gpxGame
            // 
            this.gpxGame.Controls.Add(this.lblGameFanfare);
            this.gpxGame.Controls.Add(this.chbGameFanfare);
            this.gpxGame.Controls.Add(this.nudGameDuration);
            this.gpxGame.Controls.Add(this.lblGameDuration);
            this.gpxGame.Controls.Add(this.lblGame);
            this.gpxGame.Location = new System.Drawing.Point(314, 339);
            this.gpxGame.Name = "gpxGame";
            this.gpxGame.Size = new System.Drawing.Size(253, 134);
            this.gpxGame.TabIndex = 3;
            this.gpxGame.TabStop = false;
            // 
            // lblGameFanfare
            // 
            this.lblGameFanfare.AutoSize = true;
            this.lblGameFanfare.Location = new System.Drawing.Point(16, 110);
            this.lblGameFanfare.Name = "lblGameFanfare";
            this.lblGameFanfare.Size = new System.Drawing.Size(79, 13);
            this.lblGameFanfare.TabIndex = 0;
            this.lblGameFanfare.Text = "Fanfare spielen";
            // 
            // chbGameFanfare
            // 
            this.chbGameFanfare.AutoSize = true;
            this.chbGameFanfare.Checked = true;
            this.chbGameFanfare.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chbGameFanfare.Location = new System.Drawing.Point(226, 110);
            this.chbGameFanfare.Name = "chbGameFanfare";
            this.chbGameFanfare.Size = new System.Drawing.Size(15, 14);
            this.chbGameFanfare.TabIndex = 1;
            this.chbGameFanfare.UseVisualStyleBackColor = true;
            // 
            // nudGameDuration
            // 
            this.nudGameDuration.Location = new System.Drawing.Point(121, 84);
            this.nudGameDuration.Maximum = new decimal(new int[] {
            120,
            0,
            0,
            0});
            this.nudGameDuration.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudGameDuration.Name = "nudGameDuration";
            this.nudGameDuration.Size = new System.Drawing.Size(120, 20);
            this.nudGameDuration.TabIndex = 0;
            this.nudGameDuration.Value = new decimal(new int[] {
            40,
            0,
            0,
            0});
            // 
            // lblGameDuration
            // 
            this.lblGameDuration.AutoSize = true;
            this.lblGameDuration.Location = new System.Drawing.Point(16, 86);
            this.lblGameDuration.Name = "lblGameDuration";
            this.lblGameDuration.Size = new System.Drawing.Size(57, 13);
            this.lblGameDuration.TabIndex = 0;
            this.lblGameDuration.Text = "Spieldauer";
            // 
            // lblGame
            // 
            this.lblGame.AutoSize = true;
            this.lblGame.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGame.Location = new System.Drawing.Point(6, 16);
            this.lblGame.Name = "lblGame";
            this.lblGame.Size = new System.Drawing.Size(92, 37);
            this.lblGame.TabIndex = 0;
            this.lblGame.Text = "Spiel";
            // 
            // frmOptions
            // 
            this.AcceptButton = this.btnApply;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(577, 484);
            this.Controls.Add(this.gpxGame);
            this.Controls.Add(this.btnApply);
            this.Controls.Add(this.lblSettings);
            this.Controls.Add(this.gpxScoring);
            this.Controls.Add(this.gpxColors);
            this.Controls.Add(this.gpxGameField);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = global::CollectJoe.Properties.Resources.icon;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmOptions";
            this.Text = "Optionen";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmOptions_FormClosing);
            this.gpxGameField.ResumeLayout(false);
            this.gpxGameField.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudFieldBoxesV)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudFieldBoxesH)).EndInit();
            this.gpxColors.ResumeLayout(false);
            this.gpxColors.PerformLayout();
            this.gpxScoring.ResumeLayout(false);
            this.gpxScoring.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudScoringBoxType3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudScoringBoxType2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudScoringBoxType1)).EndInit();
            this.gpxGame.ResumeLayout(false);
            this.gpxGame.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudGameDuration)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox gpxGameField;
        private System.Windows.Forms.Label lblGameField;
        private System.Windows.Forms.NumericUpDown nudFieldBoxesV;
        private System.Windows.Forms.NumericUpDown nudFieldBoxesH;
        private System.Windows.Forms.Label lblFieldBoxesV;
        private System.Windows.Forms.Label lblFieldBoxesH;
        private System.Windows.Forms.GroupBox gpxColors;
        private System.Windows.Forms.Label lblColorBoxes;
        private System.Windows.Forms.Label lblColors;
        private System.Windows.Forms.GroupBox gpxScoring;
        private System.Windows.Forms.Button btnColorsGameField;
        private System.Windows.Forms.Button btnColorsBoxes;
        private System.Windows.Forms.Label lblColorsGameField;
        private System.Windows.Forms.Button btnColorsBoxType3;
        private System.Windows.Forms.Button btnColorsBoxType2;
        private System.Windows.Forms.Button btnColorsBoxType1;
        private System.Windows.Forms.Label lblColorsBoxType3;
        private System.Windows.Forms.Label lblColorsBoxType2;
        private System.Windows.Forms.Label lblColorsBoxType1;
        private System.Windows.Forms.Label lblScoringBoxType1;
        private System.Windows.Forms.Label lblScoring;
        private System.Windows.Forms.Label lblSettings;
        private System.Windows.Forms.Label lblScoringBoxType3;
        private System.Windows.Forms.Label lblScoringBoxType2;
        private System.Windows.Forms.Button btnApply;
        private System.Windows.Forms.ColorDialog cldColorPicker;
		private System.Windows.Forms.NumericUpDown nudScoringBoxType3;
		private System.Windows.Forms.NumericUpDown nudScoringBoxType2;
		private System.Windows.Forms.NumericUpDown nudScoringBoxType1;
		private System.Windows.Forms.GroupBox gpxGame;
		private System.Windows.Forms.Label lblGameFanfare;
		private System.Windows.Forms.CheckBox chbGameFanfare;
		private System.Windows.Forms.NumericUpDown nudGameDuration;
		private System.Windows.Forms.Label lblGameDuration;
		private System.Windows.Forms.Label lblGame;
	}
}