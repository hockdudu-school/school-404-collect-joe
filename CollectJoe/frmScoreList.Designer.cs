﻿namespace CollectJoe
{
    partial class frmScoreList
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblScoreList = new System.Windows.Forms.Label();
            this.txtRangliste = new System.Windows.Forms.TextBox();
            this.btnClose = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lblScoreList
            // 
            this.lblScoreList.AutoSize = true;
            this.lblScoreList.Font = new System.Drawing.Font("Microsoft Sans Serif", 32F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblScoreList.Location = new System.Drawing.Point(13, 13);
            this.lblScoreList.Name = "lblScoreList";
            this.lblScoreList.Size = new System.Drawing.Size(212, 51);
            this.lblScoreList.TabIndex = 0;
            this.lblScoreList.Text = "Rangliste";
            // 
            // txtRangliste
            // 
            this.txtRangliste.Enabled = false;
            this.txtRangliste.Location = new System.Drawing.Point(22, 68);
            this.txtRangliste.Multiline = true;
            this.txtRangliste.Name = "txtRangliste";
            this.txtRangliste.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtRangliste.Size = new System.Drawing.Size(313, 361);
            this.txtRangliste.TabIndex = 0;
            this.txtRangliste.TabStop = false;
            // 
            // btnClose
            // 
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Location = new System.Drawing.Point(22, 435);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 0;
            this.btnClose.Text = "&Schliessen";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.BtnClose_Click);
            // 
            // frmScoreList
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnClose;
            this.ClientSize = new System.Drawing.Size(347, 470);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.txtRangliste);
            this.Controls.Add(this.lblScoreList);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = global::CollectJoe.Properties.Resources.icon;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmScoreList";
            this.Text = "Rangliste";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmScoreList_FormClosing);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblScoreList;
        private System.Windows.Forms.TextBox txtRangliste;
        private System.Windows.Forms.Button btnClose;
    }
}