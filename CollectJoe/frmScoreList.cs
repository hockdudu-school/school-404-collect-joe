﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;

namespace CollectJoe
{
    // ReSharper disable once InconsistentNaming
    public partial class frmScoreList : Form
    {
        private readonly string _saveFileLocation;

        public frmScoreList(string scorePath)
        {
            InitializeComponent();
            _saveFileLocation = scorePath;
        }

        public void RefreshScore()
        {
            txtRangliste.Text = "";

            try
            {
                var text = File.ReadAllLines(_saveFileLocation);
                var scores = new List<Tuple<string, int>>();

                foreach (var line in text)
                {
                    var splitLine = line.Split(';');

                    if (splitLine.Length == 2 && int.TryParse(splitLine[1], out var playerScore))
                        scores.Add(new Tuple<string, int>(splitLine[0], playerScore));
                }

                scores.Sort((x, y) => y.Item2.CompareTo(x.Item2));

                foreach (var playerTuple in scores)
                    txtRangliste.Text += playerTuple.Item1 + ": " + playerTuple.Item2 + Environment.NewLine;
            }
            catch (ArgumentException)
            {
                txtRangliste.Text = "Rangliste nicht verfügbar.";
            }
            catch (FileNotFoundException)
            {
                txtRangliste.Text = "";
            }
        }

        private void BtnClose_Click(object sender, EventArgs e)
        {
            Hide();
        }

        private void FrmScoreList_FormClosing(object sender, FormClosingEventArgs e)
        {
            Hide();
            e.Cancel = true;
        }
    }
}