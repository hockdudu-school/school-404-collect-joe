﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace CollectJoe
{
    // ReSharper disable once InconsistentNaming
    public partial class frmField : Form
    {
        private readonly frmOptions _formOptions = new frmOptions();
        private readonly frmEditScore _formEditScore;
        private readonly frmScoreList _formScoreList;

        private readonly Random _randomGenerator = new Random();

        private int _gameDuration;
        private int _playedTime;

        // ReSharper disable once PrivateFieldCanBeConvertedToLocalVariable
        private readonly string _highScorePath;

        private int _boxesHorizontal;
        private int _boxesVertical;

        private int _boxHeight;
        private int _boxWidth;

        private Color _boxColor;

        private readonly List<Button> _buttonsList = new List<Button>();

        // ReSharper disable once RedundantDefaultMemberInitializer
        private int _lastBoxIndex = 0;

        private readonly Dictionary<Color, int> _boxColorsDictionary = new Dictionary<Color, int>();

        private readonly int[] _gameFieldMargin = {10, 10};

        private bool _wasBoxOnThisTickAlreadyClicked;

        private int GetRemainingGameTimeInSeconds =>
            (_gameDuration - _playedTime + 500) / 1000; // Adds 500 for rounding up


        public frmField()
        {
            InitializeComponent();

            var appDirectory = AppDomain.CurrentDomain.BaseDirectory;

            _highScorePath = Path.Combine(appDirectory, "highscore.txt");

            if (!File.Exists(_highScorePath)) File.WriteAllText(_highScorePath, "");

            _formEditScore = new frmEditScore(_highScorePath);
            _formScoreList = new frmScoreList(_highScorePath);

            SetOptions();
            BuildButtonField();

            _formOptions.VisibleChanged += FormOptions_VisibleChanged;
            _formEditScore.VisibleChanged += FormEditScore_VisibleChanged; //Improvement
        }

        // ReSharper disable once MemberCanBePrivate.Global
        public void SetOptions()
        {
            _formEditScore.PlayFanfare = _formOptions.GetFanfare;

            _gameDuration = _formOptions.GetValue("nudGameDuration") * 1000;

            pnlBackground.BackColor = _formOptions.GetColor("btnColorsGameField");

            txtTime.Text = GetRemainingGameTimeInSeconds.ToString();

            _boxesHorizontal = _formOptions.GetHorizontal();
            _boxesVertical = _formOptions.GetVertical();


            var fieldHeight = pnlBackground.Height;
            var fieldWidth = pnlBackground.Width;

            var marginTopBottom = _gameFieldMargin[0];
            var marginLeftRight = _gameFieldMargin[1];

            _boxHeight = (fieldHeight - 2 * marginTopBottom) / _boxesVertical;
            _boxWidth = (fieldWidth - 2 * marginLeftRight) / _boxesHorizontal;

            _boxColor = _formOptions.GetColor("btnColorsBoxes");


            _boxColorsDictionary.Clear();

            var box1Color = _formOptions.GetColor("btnColorsBoxType1");
            var box2Color = _formOptions.GetColor("btnColorsBoxType2");
            var box3Color = _formOptions.GetColor("btnColorsBoxType3");

            var box1Value = _formOptions.GetValue("nudScoringBoxType1"); // Send nuds
            var box2Value = _formOptions.GetValue("nudScoringBoxType2");
            var box3Value = _formOptions.GetValue("nudScoringBoxType3");

            _boxColorsDictionary.Add(box1Color, box1Value);
            _boxColorsDictionary.Add(box2Color, box2Value);
            _boxColorsDictionary.Add(box3Color, box3Value);
        }

        // ReSharper disable once MemberCanBePrivate.Global
        public void StopGame()
        {
            tmrGameTick.Stop();
            if (int.Parse(txtPointsDisplay.Text) < 0)
            {
                MessageBox.Show("GAME OVER!", "", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
            else
            {
                _formEditScore.SetScore(txtPointsDisplay.Text);
                _formEditScore.ShowDialog();
            }
        }

        private void BuildButtonField()
        {
            pnlBackground.Controls.Clear();
            _buttonsList.Clear();

            var marginTopBottom = _gameFieldMargin[0];
            var marginLeftRight = _gameFieldMargin[1];

            for (var i = 0; i < _boxesHorizontal; i++)
            {
                for (var j = 0; j < _boxesVertical; j++)
                {
                    var boxName = (i * j + j).ToString();
                    var boxLocationX = marginLeftRight + i * _boxWidth;
                    var boxLocationY = marginTopBottom + j * _boxHeight;
                    var boxLocation = new Point(boxLocationX, boxLocationY);

                    var button = new Button
                    {
                        Name = boxName,
                        Height = _boxHeight,
                        Width = _boxWidth,
                        Location = boxLocation,
                        Text = "",
                        BackColor = _boxColor,
                        TabStop = false
                    };

                    button.Click += BtnBox_Click;

                    pnlBackground.Controls.Add(button);
                    _buttonsList.Add(button);
                }
            }
        }

        private void BtnBox_Click(object sender, EventArgs e)
        {
            if (_wasBoxOnThisTickAlreadyClicked) return;

            var buttonColor = ((Button) sender).BackColor;

            if (!_boxColorsDictionary.ContainsKey(buttonColor)) return;

            _wasBoxOnThisTickAlreadyClicked = true;

            var currentPoints = int.Parse(txtPointsDisplay.Text);
            _boxColorsDictionary.TryGetValue(buttonColor, out var value);

            currentPoints += value;

            txtPointsDisplay.Text = currentPoints.ToString();

            if (currentPoints < 0) StopGame();
        }

        private void FormOptions_VisibleChanged(object sender, EventArgs e)
        {
            if (((Form) sender).Visible) return;

            SetOptions();
            BuildButtonField();
        }

        //Improvement
        private void FormEditScore_VisibleChanged(object sender, EventArgs e)
        {
            if (((Form) sender).Visible || !_formEditScore.UserSaved) return;
            
            _formScoreList.RefreshScore();
            _formScoreList.ShowDialog();
        }

        private void BtnSettings_Click(object sender, EventArgs e) => _formOptions.ShowDialog();

        private void BtnStartGame_Click(object sender, EventArgs e)
        {
            _playedTime = 0;
            _lastBoxIndex = 0;
            txtPointsDisplay.Text = 0.ToString();

            _buttonsList.ForEach(button => button.BackColor = _boxColor);

            _formEditScore.ReSetNameAndScore();

            tmrGameTick.Start();
        }

        private void TmrGameTick_Tick(object sender, EventArgs e)
        {
            _playedTime += tmrGameTick.Interval;

            txtTime.Text = GetRemainingGameTimeInSeconds.ToString();

            _wasBoxOnThisTickAlreadyClicked = false;
            _buttonsList[_lastBoxIndex].BackColor = _boxColor; // Reset color BEFORE ending game

            if (_playedTime > _gameDuration)
            {
                StopGame();
                return;
            }

            var randomColorIndex = _randomGenerator.Next(_boxColorsDictionary.Count);

            var randomBoxIndex = _randomGenerator.Next(_buttonsList.Count);
            _lastBoxIndex = randomBoxIndex;

            _buttonsList[randomBoxIndex].BackColor = _boxColorsDictionary.ElementAt(randomColorIndex).Key;
        }

        private void BtnShowScores_Click(object sender, EventArgs e)
        {
            _formScoreList.RefreshScore();
            _formScoreList.ShowDialog();
        }
    }
}