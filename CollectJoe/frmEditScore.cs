﻿using System;
using System.IO;
using System.Linq;
using System.Media;
using System.Windows.Forms;

namespace CollectJoe
{
    // ReSharper disable once InconsistentNaming
    public partial class frmEditScore : Form
    {
        private readonly string _highScorePath;

        public bool PlayFanfare = false;
        public bool UserSaved;

        public frmEditScore(string highScorePath)
        {
            InitializeComponent();

            _highScorePath = highScorePath;
        }

        public void SetScore(string score) => lblPoints.Text = score;

        public void ReSetNameAndScore()
        {
            txtName.Text = "";
            lblPoints.Text = "";
        }

        private int GetHighestScore()
        {
            try
            {
                var text = File.ReadAllLines(_highScorePath);

                return text.Max(line =>
                {
                    var splitLine = line.Split(';');
                    if (splitLine.Length == 2 && int.TryParse(splitLine[1], out var playerScore)) return playerScore;
                    return 0;
                });
            }
            catch (Exception)
            {
                return 0;
            }
        }

        private void BtnSave_Click(object sender, EventArgs e)
        {
            var name = txtName.Text.Trim();

            if (name.Length == 0)
            {
                MessageBox.Show("Bitte geben Sie ihren Name ein", "Keinen Namen eingegeben", MessageBoxButtons.OK,
                    MessageBoxIcon.Exclamation);
                return;
            }

            var score = lblPoints.Text;

            var textToSave = name.Replace(";", "") + ';' + score + Environment.NewLine;

            File.AppendAllText(_highScorePath, textToSave);

            UserSaved = true;
            Hide();
        }

        private void BtnCancel_Click(object sender, EventArgs e)
        {
            UserSaved = false;
            Hide();
        }

        private void FrmEditScore_FormClosing(object sender, FormClosingEventArgs e)
        {
            UserSaved = false;
            Hide();
            e.Cancel = true;
        }

        private void FrmEditScore_VisibleChanged(object sender, EventArgs e)
        {
            if (!((Form) sender).Visible || !int.TryParse(lblPoints.Text, out var score) || !PlayFanfare) return;

            if (score > GetHighestScore())
                new SoundPlayer(Properties.Resources.fanfare).Play();
        }
    }
}