﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace CollectJoe
{
    // ReSharper disable once InconsistentNaming
    public partial class frmOptions : Form
    {
        //Improvement
        private static class EdgeValues
        {
            /* We use top/bottom instead of maximum/minimum
             * because it's easier to differenciate
             */

            internal const int VerticalTop = 10;
            internal const int VerticalTopReplace = 10;

            internal const int VerticalBottom = 1;
            internal const int VerticalBottomReplace = 10;

            internal const int HorizontalTop = 20;
            internal const int HorizontalTopReplace = 20;

            internal const int HorizontalBottom = 1;
            internal const int HorizontalBottomReplace = 10;
        }


        public frmOptions()
        {
            InitializeComponent();

            nudFieldBoxesH.Maximum = EdgeValues.HorizontalTop;
            nudFieldBoxesH.Minimum = EdgeValues.HorizontalBottom;

            nudFieldBoxesV.Maximum = EdgeValues.VerticalTop;
            nudFieldBoxesV.Minimum = EdgeValues.VerticalBottom;
        }

        public int GetHorizontal()
        {
            var readValue = nudFieldBoxesH.Value;

            if (readValue > EdgeValues.HorizontalTop) return EdgeValues.HorizontalTopReplace;
            if (readValue < EdgeValues.HorizontalBottom) return EdgeValues.HorizontalBottomReplace;

            return (int) readValue;
        }

        public int GetVertical()
        {
            var readValue = nudFieldBoxesV.Value;

            if (readValue > EdgeValues.VerticalTop) return EdgeValues.VerticalTopReplace;
            if (readValue < EdgeValues.VerticalBottom) return EdgeValues.VerticalBottomReplace;

            return (int) readValue;
        }

        public int GetValue(string inputForm)
        {
            if (!FindFormByName(inputForm, out var foundControl)) return 1;

            var formValue = foundControl.Text;

            return !int.TryParse(formValue, out var parsedNumber) ? 1 : parsedNumber;
        }

        public Color GetColor(string button)
        {
            return !FindFormByName(button, out var foundControl) ? Color.DeepPink : foundControl.BackColor;
        }

        public bool GetFanfare => chbGameFanfare.Checked;

        private void BtnColors_Click(object sender, EventArgs e)
        {
            if (cldColorPicker.ShowDialog() == DialogResult.OK)
                ((Button) sender).BackColor = cldColorPicker.Color;
        }

        private void BtnApply_Click(object sender, EventArgs e)
        {
            if (AreColorsValid())
            {
                Hide();
            }
            else
            {
                MessageBox.Show("Wählen Sie bitte keine duplizierten Farben", "Duplizierte Angabe",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
        }

        private void FrmOptions_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (AreColorsValid())
            {
                Hide();
            }
            else
            {
                MessageBox.Show("Wählen Sie bitte keine duplizierten Farben", "Duplizierte Angabe",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }

            e.Cancel = true;
        }

        private bool FindFormByName(string formName, out Control form)
        {
            form = null;

            var foundForms = Controls.Find(formName, true);

            if (foundForms.Length == 0) return false;

            form = foundForms[0];
            return true;
        }

        private bool AreColorsValid()
        {
            var hashset = new HashSet<Color>();
            return gpxColors.Controls.OfType<Button>().All(button => hashset.Add(button.BackColor));
        }
    }
}