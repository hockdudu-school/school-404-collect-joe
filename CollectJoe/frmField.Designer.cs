﻿namespace CollectJoe
{
    partial class frmField
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.pnlBackground = new System.Windows.Forms.Panel();
            this.btnStartGame = new System.Windows.Forms.Button();
            this.lblPoints = new System.Windows.Forms.Label();
            this.txtPointsDisplay = new System.Windows.Forms.TextBox();
            this.btnSettings = new System.Windows.Forms.Button();
            this.btnShowScores = new System.Windows.Forms.Button();
            this.tmrGameTick = new System.Windows.Forms.Timer(this.components);
            this.lblTime = new System.Windows.Forms.Label();
            this.txtTime = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // pnlBackground
            // 
            this.pnlBackground.Location = new System.Drawing.Point(13, 13);
            this.pnlBackground.Name = "pnlBackground";
            this.pnlBackground.Size = new System.Drawing.Size(955, 302);
            this.pnlBackground.TabIndex = 0;
            // 
            // btnStartGame
            // 
            this.btnStartGame.Location = new System.Drawing.Point(13, 322);
            this.btnStartGame.Name = "btnStartGame";
            this.btnStartGame.Size = new System.Drawing.Size(75, 23);
            this.btnStartGame.TabIndex = 0;
            this.btnStartGame.Text = "&Spiel starten";
            this.btnStartGame.UseVisualStyleBackColor = true;
            this.btnStartGame.Click += new System.EventHandler(this.BtnStartGame_Click);
            // 
            // lblPoints
            // 
            this.lblPoints.AutoSize = true;
            this.lblPoints.Location = new System.Drawing.Point(166, 327);
            this.lblPoints.Name = "lblPoints";
            this.lblPoints.Size = new System.Drawing.Size(67, 13);
            this.lblPoints.TabIndex = 0;
            this.lblPoints.Text = "Punktestand";
            // 
            // txtPointsDisplay
            // 
            this.txtPointsDisplay.Enabled = false;
            this.txtPointsDisplay.Location = new System.Drawing.Point(239, 323);
            this.txtPointsDisplay.Name = "txtPointsDisplay";
            this.txtPointsDisplay.Size = new System.Drawing.Size(100, 20);
            this.txtPointsDisplay.TabIndex = 0;
            this.txtPointsDisplay.TabStop = false;
            // 
            // btnSettings
            // 
            this.btnSettings.AutoSize = true;
            this.btnSettings.Location = new System.Drawing.Point(892, 323);
            this.btnSettings.Name = "btnSettings";
            this.btnSettings.Size = new System.Drawing.Size(80, 23);
            this.btnSettings.TabIndex = 2;
            this.btnSettings.Text = "&Einstellungen";
            this.btnSettings.UseVisualStyleBackColor = true;
            this.btnSettings.Click += new System.EventHandler(this.BtnSettings_Click);
            // 
            // btnShowScores
            // 
            this.btnShowScores.Location = new System.Drawing.Point(811, 323);
            this.btnShowScores.Name = "btnShowScores";
            this.btnShowScores.Size = new System.Drawing.Size(75, 23);
            this.btnShowScores.TabIndex = 1;
            this.btnShowScores.Text = "&Rangliste anzeigen";
            this.btnShowScores.UseVisualStyleBackColor = true;
            this.btnShowScores.Click += new System.EventHandler(this.BtnShowScores_Click);
            // 
            // tmrGameTick
            // 
            this.tmrGameTick.Interval = 1000;
            this.tmrGameTick.Tick += new System.EventHandler(this.TmrGameTick_Tick);
            // 
            // lblTime
            // 
            this.lblTime.AutoSize = true;
            this.lblTime.Location = new System.Drawing.Point(520, 326);
            this.lblTime.Name = "lblTime";
            this.lblTime.Size = new System.Drawing.Size(25, 13);
            this.lblTime.TabIndex = 3;
            this.lblTime.Text = "Zeit";
            // 
            // txtTime
            // 
            this.txtTime.Enabled = false;
            this.txtTime.Location = new System.Drawing.Point(551, 324);
            this.txtTime.Name = "txtTime";
            this.txtTime.Size = new System.Drawing.Size(100, 20);
            this.txtTime.TabIndex = 4;
            // 
            // frmField
            // 
            this.AcceptButton = this.btnStartGame;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(980, 354);
            this.Controls.Add(this.txtTime);
            this.Controls.Add(this.lblTime);
            this.Controls.Add(this.btnShowScores);
            this.Controls.Add(this.btnSettings);
            this.Controls.Add(this.txtPointsDisplay);
            this.Controls.Add(this.lblPoints);
            this.Controls.Add(this.btnStartGame);
            this.Controls.Add(this.pnlBackground);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = global::CollectJoe.Properties.Resources.icon;
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.Name = "frmField";
            this.Text = "CollectJoe";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel pnlBackground;
        private System.Windows.Forms.Button btnStartGame;
        private System.Windows.Forms.Label lblPoints;
        private System.Windows.Forms.TextBox txtPointsDisplay;
        private System.Windows.Forms.Button btnSettings;
        private System.Windows.Forms.Button btnShowScores;
        private System.Windows.Forms.Timer tmrGameTick;
		private System.Windows.Forms.Label lblTime;
		private System.Windows.Forms.TextBox txtTime;
	}
}

